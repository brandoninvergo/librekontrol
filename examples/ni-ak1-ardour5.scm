;; Copyright (C) 2019  Brandon M. Invergo <brandon@invergo.net>

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(use-modules (librekontrol core)
             (librekontrol device)
             (librekontrol input)
             (librekontrol alsa)
             ((librekontrol devices ni-ak1) #:prefix ak1:))

(configure-device
 #:alsa-name ak1:alsa-name
 #:input-name ak1:input-name
 #:remap-events (list (make-input-event ev-key key-home)
                      (make-input-event ev-key key-end)
                      (make-input-event ev-key key-space)
                      (make-input-event ev-key key-leftbrace)
                      (make-input-event ev-key key-rightbrace)
                      (make-input-event ev-key key-leftshift))
 #:controls (list (list ak1:left remap-button
                        ;; Transport: Go to Start
                        #:event-code key-home)
                  (list ak1:middle (make-remap-button-toggle)
                        ;; Transport: Start/Stop
                        #:event-code key-space)
                  (list ak1:right remap-button
                        ;; Transport: Go to End
                        #:event-code key-end)
                  (list ak1:knob (make-abs-knob-to-button)
                        ;; Bind Shift-{ & Shift-} to, e.g., Scroll
                        ;; Backward/Forward
                        #:neg-event-code key-leftbrace
                        #:pos-event-code key-rightbrace
                        #:neg-mod-keys (list key-leftshift)
                        #:pos-mod-keys (list key-leftshift)
                        #:knob-max (ak1:knob-max)
                        #:invert #t))
 #:init (list (lambda ()
                (turn-off-ctls
                 (map control-alsa-ctl
                      (list ak1:left ak1:middle ak1:right ak1:knob)))))
 #:exit-hooks (list (lambda ()
                      (turn-off-ctls
                       (map control-alsa-ctl
                            (list ak1:left ak1:middle ak1:right ak1:knob))))))


