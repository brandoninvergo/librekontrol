/* 
 * lk-device.h --- 
 * 
 * Copyright (C) 2016, 2017, 2019 Brandon Invergo <brandon@invergo.net>
 * 
 * Author: Brandon Invergo <brandon@invergo.net>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef LK_DEVICE_H
#define LK_DEVICE_H

#include <config.h>
#include <alsa/asoundlib.h>
#include <error.h>
#include <stdlib.h>
#include <errno.h>
#include <stdbool.h>
#include <glob.h>
#include <fcntl.h>
#include <unistd.h>
#include <libevdev/libevdev.h>
#include <libevdev/libevdev-uinput.h>
#include <libintl.h>
#include <pthread.h>
#include <linux/input.h>

typedef struct
{
  char str_id[64];
  char id[64];
  snd_ctl_t *ctl_handle;
  snd_seq_t *seq_handle;
  int seq_read_port;
  int seq_write_port;
  struct libevdev *input_dev;
  struct libevdev *remap_dev;
  int input_fd;
  struct libevdev_uinput *uidev;
} lk_device_t;

typedef struct
{
  lk_device_t *device;
  char id[64];
  snd_ctl_elem_id_t *elem_id;
  snd_ctl_elem_info_t *info;
  snd_ctl_elem_value_t *value;
  pthread_mutex_t mutex;
  pthread_mutexattr_t mutexattr;
} lk_device_ctl_t;

void lk_init ();
void lk_cleanup ();
int open_device (lk_device_t *device, const char *controller_name,
                 const char *input_name, bool open_midi_seq,
                 bool open_remap_evdev);
int close_device (lk_device_t *device);
int open_control (lk_device_ctl_t *ctl, lk_device_t *device,
                  const char *ctl_id_str);
int close_control (lk_device_ctl_t *ctl);
int set_boolean_ctl (lk_device_ctl_t *ctl, bool value);
bool get_boolean_ctl (lk_device_ctl_t *ctl);
int toggle_boolean_ctl (lk_device_ctl_t *ctl);
int set_integer_ctl (const lk_device_ctl_t *ctl, long int value);
long int get_integer_ctl (const lk_device_ctl_t *ctl);
long int integer_ctl_max (const lk_device_ctl_t *ctl);
long int integer_ctl_min (const lk_device_ctl_t *ctl);
int abs_in_max (const lk_device_t *device, unsigned int code);
int abs_in_min (const lk_device_t *device, unsigned int code);
void send_midi_keypress (const lk_device_t *device, unsigned char channel,
                         unsigned char note, unsigned char velocity);
void send_midi_note (const lk_device_t *device, unsigned char channel,
                     unsigned char note, unsigned char velocity,
                     unsigned int dur);
void send_midi_noteon (const lk_device_t *device, unsigned char channel,
                       unsigned char note, unsigned char velocity);
void send_midi_noteoff (const lk_device_t *device, unsigned char channel,
                        unsigned char note, unsigned char velocity);
void send_midi_pgmchange (const lk_device_t *device, unsigned char channel,
                          signed int value);
void send_midi_pitchbend (const lk_device_t *device, unsigned char channel,
                          signed int value);
void send_midi_control (const lk_device_t *device, unsigned char channel,
                        unsigned int param, signed int value);
int remap_enable_event (lk_device_t *device, unsigned int type,
                         unsigned int code);
int remap_disable_event (lk_device_t *device, unsigned int type,
                          unsigned int code);
int remap_set_abs_info (lk_device_t *device, unsigned int code, int min,
                         int max, int resolution, int fuzz, int flat);
int finalize_remap_dev (lk_device_t *device);
int send_remap_event (lk_device_t *device, unsigned int type, unsigned int code,
                       int value);

#endif
