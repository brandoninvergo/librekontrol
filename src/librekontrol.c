/* 
 * librekontrol.c --- 
 * 
 * Copyright (C) 2016, 2017, 2018, 2019 Brandon Invergo <brandon@invergo.net>
 * 
 * Author: Brandon Invergo <brandon@invergo.net>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#define _GNU_SOURCE

#include <config.h>
#include <argp.h>
#include <error.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <libguile.h>
#include <pthread.h>
#include <libintl.h>
#include "lk-device.h"
#include "librekontrol-core.h"

#define _(String) gettext (String)
#define gettext_noop(String) String
#define N_(String) gettext_noop (String)

const char *program_name = PACKAGE_NAME;
const char *argp_program_version = PACKAGE_STRING;
const char *argp_program_bug_address = PACKAGE_BUGREPORT;
static char doc[] =
  "librekontrol -- A programmable controller editor"
  "\v"
  "By default, the program loads an initialization script at "
  "$XDG_CONFIG_HOME/librekontrol/init.scm.  If the environment variable "
  "$XDG_CONFIG_HOME is not defined, $HOME/.config/librekontrol/init.scm is "
  "loaded. To specify a different file, use the --load-file option.\n"
  "\nCopyright (C) 2016, 2017, 2018, 2019 Brandon Invergo.\n"
  "License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>.\n"
  "This is free software: you are free to change and redistribute it.\n"
  "There is NO WARRANTY, to the extent permitted by law.";
static char args_doc[] = "";

static struct argp_option options[] = {
  {"load-file", 'f', "PATH", 0, "Load the specified file instead of the "
   "default init.scm file", 0},
  {"debug", 'd', 0, 0, "Print debug information", 0},
  {(const char *)NULL, 0, (const char *)NULL, 0, (const char *)NULL, 0}
};

struct arguments
{
  char *init_file;
  bool debug;
};

static struct arguments arguments;

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct arguments *arguments = state->input;
  switch (key)
    {
    case 'd':
      arguments->debug = true;
      break;
    case 'f':
      if (strlen (arg) == 0)
        argp_usage (state);
      arguments->init_file = arg;
      break;
    default:
      return ARGP_ERR_UNKNOWN;
    }
  return 0;
}

static struct argp argp = {options, parse_opt, args_doc, doc,
                           (const struct argp_child *)NULL,
                           NULL, (const char *)NULL};

void
resync_events (lk_device_t *device)
{
  struct input_event ev;
  int status = LIBEVDEV_READ_STATUS_SYNC;

  while (status == LIBEVDEV_READ_STATUS_SYNC)
    {
      status = libevdev_next_event (device->input_dev, LIBEVDEV_READ_FLAG_SYNC,
                                    &ev);
      if (status < 0)
        {
          if (status != -EAGAIN)
            {
              error (0, errno, _("Failed grabbing next event: %s"),
                     strerror (status));
            }
          if (arguments.debug)
            {
              printf (_("State change since SYN_DROPPED: "
                        "%s %s %d\n"), libevdev_event_type_get_name (ev.type),
                      libevdev_event_code_get_name (ev.type, ev.code),
                      ev.value);
            }
          return;
        }
    }
}

void
build_init_path (char *init_path)
{
  bool nonenv = false;

  char *config_home = getenv ("XDG_CONFIG_HOME");
  if (!config_home || strlen (config_home) == 0)
    {
      char *home = getenv ("HOME");
      if (home)
        {
          config_home = (char *)malloc ((strlen (home)+9)*sizeof (char));
          nonenv = true;
          if (!config_home)
            {
              error (EXIT_FAILURE, errno, _("Memory exhausted"));
            }
          strcpy (config_home, home);
          strncat (config_home, "/.config", 9);
          config_home[strlen (home)+8] = '\0';
        }
      else
        {
          return;
        }
    }
  char *config_dir = (char *)malloc ((strlen (config_home)+14)*sizeof (char));
  if (!config_dir)
    {
      error (EXIT_FAILURE, errno, _("Memory exhausted"));
    }
  strcpy (config_dir, config_home);
  strncat (config_dir, "/librekontrol", 14);
  config_dir[strlen (config_home)+13] = '\0';
  if (!init_path)
    {
      error (EXIT_FAILURE, errno, _("Memory exhausted"));
    }
  strcpy (init_path, config_dir);
  strncat (init_path, "/init.scm", 10);
  init_path[strlen (config_dir)+9] = '\0';
  if (nonenv)
    free (config_home);
  free (config_dir);
}

static void*
read_init (void *data)
{
  char *init_path = (char *)data;
  if (access (init_path, R_OK))
    {
      error (EXIT_FAILURE, 0, _("Cannot read init file %s"), init_path);
    }
  scm_c_primitive_load ((char *)data);
  return NULL;
}

static void*
event_loop (void *data)
{
  lk_device_t *device = (lk_device_t *)data;
  struct input_event ev;
  int status;
  struct pollfd fds[1];
  fds[0].fd = device->input_fd;
  fds[0].events = POLLIN;

  while (true)
    {
      while (!poll (fds, 1, -1)) 0;
      do
        {
          status = libevdev_next_event
            (device->input_dev, LIBEVDEV_READ_FLAG_NORMAL|LIBEVDEV_READ_FLAG_BLOCKING,
             &ev);
          if (status < 0)
            {
              if (status != -EAGAIN)
                {
                  error (0, errno, _("Failed grabbing next event: %s"),
                         strerror (status));
                }
            }
          else if (status == LIBEVDEV_READ_STATUS_SYNC)
            {
              resync_events (device);
            }
          else if (status == LIBEVDEV_READ_STATUS_SUCCESS)
            {
              if (ev.type == EV_SYN)
                continue;
              handle_event (device->id, ev.type, ev.code, ev.value);
              if (arguments.debug)
                {
                  printf ("%s event: %s\t'(%d . %d)\t%d\n",
                          libevdev_event_type_get_name (ev.type),
                          libevdev_event_code_get_name (ev.type, ev.code),
                          ev.type, ev.code, ev.value);
                }
            }
        }
      while (status == LIBEVDEV_READ_STATUS_SYNC ||
             status == LIBEVDEV_READ_STATUS_SUCCESS ||
             status == -EAGAIN);
    }
  return NULL;
}

static void*
idle_loop (void *data)
{
  lk_device_t *device = (lk_device_t *)data;
  unsigned int idle_wait = get_idle_wait (device->id);
  if (idle_wait == 0)
    {
      return NULL;
    }
  while (true)
    {
      usleep (idle_wait);
      run_idle_hook (device->id);
    }
  return NULL;
}

static void
run_exit_hooks (int sig)
{
  for (size_t i=0; i<lk_num_devices; i++)
    {
      run_exit_hook (lk_device_list[i]->id);
    }
  lk_scm_cleanup ();
  signal (sig, SIG_DFL);
  raise (sig);
}

void *
launch_guile_event_loop (void *threaddata)
{
  lk_device_t *device = (lk_device_t *)threaddata;
  scm_with_guile (event_loop, device);
  pthread_exit (NULL);
}

void *
launch_guile_idle_loop (void *threaddata)
{
  lk_device_t *device = (lk_device_t *)threaddata;
  scm_with_guile (idle_loop, device);
  pthread_exit (NULL);
}

int
main (int argc, char **argv)
{
  /* setlocale (LC_ALL, ""); */
  bindtextdomain (PACKAGE, LOCALEDIR);
  textdomain (PACKAGE);

  char *init_path = (char *)malloc (512*sizeof (char));
  build_init_path (init_path);
  arguments.init_file = NULL;
  arguments.debug = false;

  error_t argp_result = argp_parse (&argp, argc, argv, 0, 0, &arguments);
  if (argp_result)
    error (EXIT_FAILURE, argp_result, _("Failed to parse arguments"));

  lk_init ();
  scm_with_guile (init_lk_scm, NULL);

  if (!arguments.init_file)
    {
      scm_with_guile (read_init, init_path);
    }
  else
    {
      scm_with_guile (read_init, arguments.init_file);
    }

  signal (SIGTERM, run_exit_hooks);
  signal (SIGINT, run_exit_hooks);

  pthread_t *threads = (pthread_t *)malloc (lk_num_devices * sizeof (pthread_t));
  if (!threads)
    {
      error (EXIT_FAILURE, errno, _("Memory exhausted"));
    }
  pthread_t *idle_threads = (pthread_t *)malloc (lk_num_devices * sizeof (pthread_t));
  if (!idle_threads)
    {
      error (EXIT_FAILURE, errno, _("Memory exhausted"));
    }
  pthread_attr_t attr;
  pthread_attr_init (&attr);
  pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
  for (size_t i=0; i<lk_num_devices; i++)
    {
      int ev_thread_rc = pthread_create (&threads[i], &attr,
                                         launch_guile_event_loop,
                                         lk_device_list[i]);
      if (ev_thread_rc)
        {
          error (EXIT_FAILURE, errno, _("Error creating thread"));
        }
      int idle_thread_rc = pthread_create (&idle_threads[i], &attr,
                                           launch_guile_idle_loop,
                                           lk_device_list[i]);
      if (idle_thread_rc)
        {
          error (EXIT_FAILURE, errno, _("Error creating thread"));
        }
    }

  pthread_attr_destroy(&attr);

  for (size_t i=0; i<lk_num_devices; i++)
    {
      int ev_thread_rc = pthread_join (threads[i], NULL);
      switch (ev_thread_rc)
        {
        case EDEADLK:
          error (0, errno, _("Deadlock when joining main thread"));
          break;
        case EINVAL:
          error (0, errno, _("Thread not joinable"));
          break;
        case ESRCH:
          error (0, errno, _("Thread not found"));
          break;
        }
      int idle_thread_rc = pthread_join (idle_threads[i], NULL);
      switch (idle_thread_rc)
        {
        case EDEADLK:
          error (0, errno, _("Deadlock when joining main thread"));
          break;
        case EINVAL:
          error (0, errno, _("Thread not joinable"));
          break;
        case ESRCH:
          error (0, errno, _("Thread not found"));
          break;
        }
    }
  lk_scm_cleanup ();
  lk_cleanup ();
  free (threads);
  free (idle_threads);
  free (init_path);
  exit (EXIT_SUCCESS);
}
