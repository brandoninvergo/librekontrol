;; Copyright (C) 2019  Brandon M. Invergo <brandon@invergo.net>

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (librekontrol alsa)
  #:use-module (srfi srfi-9)
  #:export (make-alsa-ctl
            alsa-ctl?
            alsa-ctl-numid
            alsa-ctl-type
            alsa-ctl-descr
            set-alsa-ctl-descr!
            define-alsa-ctl
            open-alsa-ctl
            set-ctl
            toggle-ctl
            turn-off-ctls))

(define-record-type <alsa-ctl>
  (make-alsa-ctl numid type descr)
  alsa-ctl?
  (numid alsa-ctl-numid)
  (type alsa-ctl-type)
  (descr alsa-ctl-descr set-alsa-ctl-descr!))

(define-syntax-rule (define-alsa-ctl symbol numid type)
  "Bind SYMBOL to the numeric ID NUMID of an ALSA CTL (LED, etc)."
  (define symbol (make-alsa-ctl numid type #f)))

(define-syntax-rule (open-alsa-ctl alsa-ctl device)
  "Open ALSA-CTL in association with DEVICE."
  (set-alsa-ctl-descr! alsa-ctl (open-ctl device (alsa-ctl-numid alsa-ctl))))

(define-syntax-rule (set-ctl alsa-ctl new-value)
  "Set an ALSA CTL to NEW-VALUE, checking if it is CTL open, and be
flexible and reasonable about whether the CTL is boolean- or
integer-based versus NEW-VALUE's type.  For example, setting an
integer CTL to #t simply sets it to its maximum value."
  (let* ((have-ctl (and (not (boolean? alsa-ctl))
                        (not (boolean? (alsa-ctl-descr alsa-ctl)))))
         (boolean-ctl (and have-ctl (eq? (alsa-ctl-type alsa-ctl)
                                         'boolean)))
         (ctl-descr (if have-ctl (alsa-ctl-descr alsa-ctl) #f)))
    (when have-ctl
      (if boolean-ctl
          (if (boolean? new-value)
              (set-boolean-ctl ctl-descr new-value)
              (if (= 0 new-value)
                  (set-boolean-ctl ctl-descr #f)
                  (set-boolean-ctl ctl-descr #t)))
          (if (boolean? new-value)
              (if new-value
                  (set-integer-ctl ctl-descr (integer-ctl-max ctl-descr))
                  (set-integer-ctl ctl-descr (integer-ctl-min ctl-descr)))
              (set-integer-ctl ctl-descr new-value))))))

(define-syntax-rule (toggle-ctl alsa-ctl)
  "Toggle an ALSA CTL, checking if it is CTL open, and be flexible and
reasonable about whether the CTL is boolean- or integer-based.  For
example. Toggling an integer CTL sets it to its minimum or maximum
value, depending on whether its current value is greater than its
minimum."
  (let* ((have-ctl (and (not (boolean? alsa-ctl))
                        (not (boolean? (alsa-ctl-descr alsa-ctl)))))
         (boolean-ctl (and have-ctl (eq? (alsa-ctl-type alsa-ctl)
                                         'boolean)))
         (ctl-descr (alsa-ctl-descr alsa-ctl)))
    (when have-ctl
      (if boolean-ctl
          (toggle-boolean-ctl ctl-descr)
          (let ((cur-value (get-integer-ctl ctl-descr))
                (min-value (integer-ctl-min ctl-descr)))
            (if (= cur-value min-value)
               (set-integer-ctl ctl-descr (integer-ctl-max ctl-descr))
               (set-integer-ctl ctl-descr min-value)))))))

(define-syntax-rule (turn-off-ctls ctls)
  "Turn off (or set to minimum value) all ctls in CTLS"
  (map (lambda (ctl) (set-ctl ctl #f)) ctls))
