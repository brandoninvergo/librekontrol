;; Copyright (C) 2019  Brandon M. Invergo <brandon@invergo.net>

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (librekontrol devices ni-kontrolx1)
  #:use-module (librekontrol input)
  #:use-module (librekontrol alsa)
  #:use-module (librekontrol device)
  #:export (alsa-name
            input-name
            led-fx-a-on
            led-fx-a-1
            led-fx-a-2
            led-fx-a-3
            led-fx-b-on
            led-fx-b-1
            led-fx-b-2
            led-fx-b-3
            pot-fx-a-dry/wet
            pot-fx-a-1
            pot-fx-a-2
            pot-fx-a-3
            pot-fx-b-dry/wet
            pot-fx-b-1
            pot-fx-b-2
            pot-fx-b-3
            led-hotcue
            led-shift-white
            led-shift-green
            led-deck-a-fx1
            led-deck-a-fx2
            led-deck-a-in
            led-deck-a-out
            led-deck-a-<beat
            led-deck-a-beat>
            led-deck-a-cue
            led-deck-a-cup
            led-deck-a-play
            led-deck-a-sync
            knob-deck-a-1
            knob-deck-a-2
            led-deck-b-fx1
            led-deck-b-fx2
            led-deck-b-in
            led-deck-b-out
            led-deck-b-<beat
            led-deck-b-beat>
            led-deck-b-cue
            led-deck-b-cup
            led-deck-b-play
            led-deck-b-sync
            knob-deck-b-1
            knob-deck-b-2
            button-unknown-1
            button-unknown-2
            button-unknown-3
            button-unknown-4
            button-unknown-5
            button-unknown-6
            button-unknown-7
            button-unknown-8
            button-unknown-9
            button-unknown-10
            button-unknown-11
            button-unknown-12
            button-unknown-13
            button-unknown-14
            button-unknown-15
            button-unknown-16
            button-unknown-17
            button-unknown-18
            button-unknown-19
            button-unknown-20
            button-unknown-21
            button-unknown-22
            button-unknown-23
            button-unknown-24
            button-unknown-25
            button-unknown-26
            button-unknown-27
            button-unknown-28
            button-unknown-29
            button-unknown-30
            button-unknown-31
            button-unknown-32
            button-unknown-33
            button-unknown-34
            button-unknown-35
            button-unknown-36
            button-unknown-37
            button-unknown-38
            button-unknown-39
            button-unknown-40
            pot-max
            knob-max))

(define alsa-name "TraktorKontrolX")
(define input-name "Traktor Kontrol X1")

(define-syntax-rule (button x)
  (+ x btn-misc))

;; FX
(define-alsa-ctl led-fx-a-on 1 'integer)
(define-alsa-ctl led-fx-a-1 2 'integer)
(define-alsa-ctl led-fx-a-2 3 'integer)
(define-alsa-ctl led-fx-a-3 4 'integer)
(define-alsa-ctl led-fx-b-on 5 'integer)
(define-alsa-ctl led-fx-b-1 6 'integer)
(define-alsa-ctl led-fx-b-2 7 'integer)
(define-alsa-ctl led-fx-b-3 8 'integer)

(define-input-event pot-fx-a-dry/wet ev-abs abs-hat0x)
(define-input-event pot-fx-a-1 ev-abs abs-hat0y)
(define-input-event pot-fx-a-2 ev-abs abs-hat1x)
(define-input-event pot-fx-a-3 ev-abs abs-hat1y)
(define-input-event pot-fx-b-dry/wet ev-abs abs-hat2x)
(define-input-event pot-fx-b-1 ev-abs abs-hat2y)
(define-input-event pot-fx-b-2 ev-abs abs-hat3x)
(define-input-event pot-fx-b-3 ev-abs abs-hat3y)

;; Middle area
(define-alsa-ctl led-hotcue 9 'integer)
(define-alsa-ctl led-shift-white 10 'integer)
(define-alsa-ctl led-shift-green 11 'integer)

;; Deck A
(define-alsa-ctl led-deck-a-fx1 12 'integer)
(define-alsa-ctl led-deck-a-fx2 13 'integer)
(define-alsa-ctl led-deck-a-in 14 'integer)
(define-alsa-ctl led-deck-a-out 15 'integer)
(define-alsa-ctl led-deck-a-<beat 16 'integer)
(define-alsa-ctl led-deck-a-beat> 17 'integer)
(define-alsa-ctl led-deck-a-cue 18 'integer)
(define-alsa-ctl led-deck-a-cup 19 'integer)
(define-alsa-ctl led-deck-a-play 20 'integer)
(define-alsa-ctl led-deck-a-sync 21 'integer)

(define-input-event knob-deck-a-1 ev-abs abs-x)
(define-input-event knob-deck-a-2 ev-abs abs-y)

;; Deck B
(define-alsa-ctl led-deck-b-fx1 22 'integer)
(define-alsa-ctl led-deck-b-fx2 23 'integer)
(define-alsa-ctl led-deck-b-in 24 'integer)
(define-alsa-ctl led-deck-b-out 25 'integer)
(define-alsa-ctl led-deck-b-<beat 26 'integer)
(define-alsa-ctl led-deck-b-beat> 27 'integer)
(define-alsa-ctl led-deck-b-cue 28 'integer)
(define-alsa-ctl led-deck-b-cup 29 'integer)
(define-alsa-ctl led-deck-b-play 30 'integer)
(define-alsa-ctl led-deck-b-sync 31 'integer)

(define-input-event knob-deck-b-1 ev-abs abs-z)
(define-input-event knob-deck-b-2 ev-abs abs-misc)

;; 40 buttons of unknown description.  They start from key code
;; BTN_MISC (256)
(define-input-event button-unknown-1 ev-key (button 0))
(define-input-event button-unknown-2 ev-key (button 1))
(define-input-event button-unknown-3 ev-key (button 2))
(define-input-event button-unknown-4 ev-key (button 3))
(define-input-event button-unknown-5 ev-key (button 4))
(define-input-event button-unknown-6 ev-key (button 5))
(define-input-event button-unknown-7 ev-key (button 6))
(define-input-event button-unknown-8 ev-key (button 7))
(define-input-event button-unknown-9 ev-key (button 8))
(define-input-event button-unknown-10 ev-key (button 9))
(define-input-event button-unknown-11 ev-key (button 10))
(define-input-event button-unknown-12 ev-key (button 11))
(define-input-event button-unknown-13 ev-key (button 12))
(define-input-event button-unknown-14 ev-key (button 13))
(define-input-event button-unknown-15 ev-key (button 14))
(define-input-event button-unknown-16 ev-key (button 15))
(define-input-event button-unknown-17 ev-key (button 16))
(define-input-event button-unknown-18 ev-key (button 17))
(define-input-event button-unknown-19 ev-key (button 18))
(define-input-event button-unknown-20 ev-key (button 19))
(define-input-event button-unknown-21 ev-key (button 20))
(define-input-event button-unknown-22 ev-key (button 21))
(define-input-event button-unknown-23 ev-key (button 22))
(define-input-event button-unknown-24 ev-key (button 23))
(define-input-event button-unknown-25 ev-key (button 24))
(define-input-event button-unknown-26 ev-key (button 25))
(define-input-event button-unknown-27 ev-key (button 26))
(define-input-event button-unknown-28 ev-key (button 27))
(define-input-event button-unknown-29 ev-key (button 28))
(define-input-event button-unknown-30 ev-key (button 29))
(define-input-event button-unknown-31 ev-key (button 30))
(define-input-event button-unknown-32 ev-key (button 31))
(define-input-event button-unknown-33 ev-key (button 32))
(define-input-event button-unknown-34 ev-key (button 33))
(define-input-event button-unknown-35 ev-key (button 34))
(define-input-event button-unknown-36 ev-key (button 35))
(define-input-event button-unknown-37 ev-key (button 36))
(define-input-event button-unknown-38 ev-key (button 37))
(define-input-event button-unknown-39 ev-key (button 38))
(define-input-event button-unknown-40 ev-key (button 39))

;; Constants
(define pot-max (make-input-max-parameter 4096))
(define knob-max (make-input-max-parameter 15))
