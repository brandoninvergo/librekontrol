;; Copyright (C) 2019  Brandon M. Invergo <brandon@invergo.net>

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (librekontrol devices ni-kore)
  #:use-module (librekontrol input)
  #:use-module (librekontrol alsa)
  #:use-module (librekontrol device)
  #:export (alsa-name
            input-name
            led-f1
            led-f2
            led-f3
            led-f4
            led-f5
            led-f6
            led-f7
            led-f8
            led-touch1
            led-touch2
            led-touch3
            led-touch4
            led-touch5
            led-touch6
            led-touch7
            led-touch8
            button-1
            button-2
            button-3
            button-4
            button-5
            button-6
            button-7
            button-8
            button-knob-1
            button-knob-2
            button-knob-3
            button-knob-4
            button-knob-5
            button-knob-6
            button-knob-7
            button-knob-8
            knob-1
            knob-2
            knob-3
            knob-4
            knob-5
            knob-6
            knob-7
            knob-8
            led-left
            led-right
            led-up
            led-down
            led-stop
            led-play
            led-record
            led-listen
            led-lcd
            button-right
            button-down
            button-up
            button-left
            button-listen
            button-record
            button-playpause
            button-stop
            led-menu
            led-sound
            led-esc
            led-view
            led-enter
            led-control
            button-menu
            button-control
            button-enter
            button-view
            button-esc
            button-sound
            knob-rotary
            knob-input
            knob-phones
            knob-output
            button-lcd-backlight
            knob-max
            knob-volume-max
            knob-rotary-max))

(define alsa-name "KoreController")
(define input-name "Kore Controller")

;; Left side
(define-alsa-ctl led-f1 1 'integer)
(define-alsa-ctl led-f2 2 'integer)
(define-alsa-ctl led-f3 3 'integer)
(define-alsa-ctl led-f4 4 'integer)
(define-alsa-ctl led-f5 5 'integer)
(define-alsa-ctl led-f6 6 'integer)
(define-alsa-ctl led-f7 7 'integer)
(define-alsa-ctl led-f8 8 'integer)
(define-alsa-ctl led-touch1 9 'integer)
(define-alsa-ctl led-touch2 10 'integer)
(define-alsa-ctl led-touch3 11 'integer)
(define-alsa-ctl led-touch4 12 'integer)
(define-alsa-ctl led-touch5 13 'integer)
(define-alsa-ctl led-touch6 14 'integer)
(define-alsa-ctl led-touch7 15 'integer)
(define-alsa-ctl led-touch8 16 'integer)

(define-input-event button-1 ev-key btn-4)
(define-input-event button-2 ev-key btn-3)
(define-input-event button-3 ev-key btn-2)
(define-input-event button-4 ev-key btn-1)
(define-input-event button-5 ev-key btn-8)
(define-input-event button-6 ev-key btn-7)
(define-input-event button-7 ev-key btn-6)
(define-input-event button-8 ev-key btn-5)
(define-input-event button-knob-1 ev-key key-brl-dot4)
(define-input-event button-knob-2 ev-key key-brl-dot3)
(define-input-event button-knob-3 ev-key key-brl-dot2)
(define-input-event button-knob-4 ev-key key-brl-dot1)
(define-input-event button-knob-5 ev-key key-brl-dot8)
(define-input-event button-knob-6 ev-key key-brl-dot7)
(define-input-event button-knob-7 ev-key key-brl-dot6)
(define-input-event button-knob-8 ev-key key-brl-dot5)
(define-input-event knob-1 ev-abs abs-hat0x)
(define-input-event knob-2 ev-abs abs-hat0y)
(define-input-event knob-3 ev-abs abs-hat1x)
(define-input-event knob-4 ev-abs abs-hat1y)
(define-input-event knob-5 ev-abs abs-hat2x)
(define-input-event knob-6 ev-abs abs-hat2y)
(define-input-event knob-7 ev-abs abs-hat3x)
(define-input-event knob-8 ev-abs abs-hat3y)

;; Middle
(define-alsa-ctl led-left 17 'integer)
(define-alsa-ctl led-right 18 'integer)
(define-alsa-ctl led-up 19 'integer)
(define-alsa-ctl led-down 20 'integer)
(define-alsa-ctl led-stop 21 'integer)
(define-alsa-ctl led-play 22 'integer)
(define-alsa-ctl led-record 23 'integer)
(define-alsa-ctl led-listen 24 'integer)
(define-alsa-ctl led-lcd 25 'integer)

(define-input-event button-right ev-key key-right)
(define-input-event button-down ev-key key-down)
(define-input-event button-up ev-key key-up)
(define-input-event button-left ev-key key-left)
(define-input-event button-listen ev-key key-sound)
(define-input-event button-record ev-key key-record)
(define-input-event button-playpause ev-key key-playpause)
(define-input-event button-stop ev-key key-stop)

;; Right side
(define-alsa-ctl led-menu 26 'integer)
(define-alsa-ctl led-sound 27 'integer)
(define-alsa-ctl led-esc 28 'integer)
(define-alsa-ctl led-view 29 'integer)
(define-alsa-ctl led-enter 30 'integer)
(define-alsa-ctl led-control 31 'integer)

(define-input-event button-menu ev-key key-fn-f1)
(define-input-event button-control ev-key key-fn-f2)
(define-input-event button-enter ev-key key-fn-f3)
(define-input-event button-view ev-key key-fn-f4)
(define-input-event button-esc ev-key key-fn-f5)
(define-input-event button-sound ev-key key-fn-f6)
(define-input-event knob-rotary ev-abs abs-misc)

;; Top area
(define-input-event knob-input ev-abs abs-x)
(define-input-event knob-phones ev-abs abs-y)
(define-input-event knob-output ev-abs abs-z)
(define-input-event button-lcd-backlight ev-key key-fn-f7)

;; Constants
(define knob-max (make-input-max-parameter 999))
(define knob-volume-max (make-input-max-parameter 4096))
(define knob-rotary-max (make-input-max-parameter 255))
