;; Copyright (C) 2019  Brandon M. Invergo <brandon@invergo.net>

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (librekontrol devices ni-kontrols4)
  #:use-module (librekontrol input)
  #:use-module (librekontrol alsa)
  #:use-module (librekontrol device)
  #:export (alsa-name
            input-name
            led-master-quant
            led-master-headphone
            led-master-master
            led-master-snap
            led-master-warning
            led-master-master-button
            led-master-snap-button
            led-master-rec
            led-master-size
            led-master-quant-button
            led-master-browser-button
            led-master-play-button
            led-master-undo-button
            knob-loop-dry/wet
            fader-crossfader
            knob-mic-volume
            knob-cue-mix
            led-chan-a->
            led-chan-a-<
            led-chan-a-meter-1
            led-chan-a-meter-2
            led-chan-a-meter-3
            led-chan-a-meter-4
            led-chan-a-meter-5
            led-chan-a-meter-6
            led-chan-a-meter-clip
            led-chan-a-active
            led-chan-a-cue
            led-chan-a-fx1
            led-chan-a-fx2
            fader-chan-a
            knob-chan-a-eq-filter
            fader-chan-a-eq-low
            fader-chan-a-eq-mid
            fader-chan-a-eq-hi
            led-chan-b->
            led-chan-b-<
            led-chan-b-meter-1
            led-chan-b-meter-2
            led-chan-b-meter-3
            led-chan-b-meter-4
            led-chan-b-meter-5
            led-chan-b-meter-6
            led-chan-b-meter-clip
            led-chan-b-active
            led-chan-b-cue
            led-chan-b-fx1
            led-chan-b-fx2
            fader-chan-b
            knob-chan-b-eq-filter
            fader-chan-b-eq-low
            fader-chan-b-eq-mid
            fader-chan-b-eq-hi
            led-chan-c->
            led-chan-c-<
            led-chan-c-meter-1
            led-chan-c-meter-2
            led-chan-c-meter-3
            led-chan-c-meter-4
            led-chan-c-meter-5
            led-chan-c-meter-6
            led-chan-c-meter-clip
            led-chan-c-active
            led-chan-c-cue
            led-chan-c-fx1
            led-chan-c-fx2
            fader-chan-c
            knob-chan-c-eq-filter
            fader-chan-c-eq-low
            fader-chan-c-eq-mid
            fader-chan-c-eq-hi
            led-chan-d->
            led-chan-d-<
            led-chan-d-meter-1
            led-chan-d-meter-2
            led-chan-d-meter-3
            led-chan-d-meter-4
            led-chan-d-meter-5
            led-chan-d-meter-6
            led-chan-d-meter-clip
            led-chan-d-active
            led-chan-d-cue
            led-chan-d-fx1
            led-chan-d-fx2
            fader-chan-d
            knob-chan-d-eq-filter
            fader-chan-d-eq-low
            fader-chan-d-eq-mid
            fader-chan-d-eq-hi
            led-deck-a-1-blue
            led-deck-a-1-green
            led-deck-a-2-blue
            led-deck-a-2-green
            led-deck-a-3-blue
            led-deck-a-3-green
            led-deck-a-4-blue
            led-deck-a-4-green
            led-deck-a-load
            led-deck-a-deck-c-button
            led-deck-a-in
            led-deck-a-out
            led-deck-a-shift
            led-deck-a-sync
            led-deck-a-cue
            led-deck-a-play
            led-deck-a-tempo-up
            led-deck-a-tempo-down
            led-deck-a-master
            led-deck-a-keylock
            led-deck-a-deck-a
            led-deck-a-deck-c
            led-deck-a-samples
            led-deck-a-on-air
            led-deck-a-sample-1
            led-deck-a-sample-2
            led-deck-a-sample-3
            led-deck-a-sample-4
            led-deck-a-digit-1-a
            led-deck-a-digit-1-b
            led-deck-a-digit-1-c
            led-deck-a-digit-1-d
            led-deck-a-digit-1-e
            led-deck-a-digit-1-f
            led-deck-a-digit-1-g
            led-deck-a-digit-1-dot
            led-deck-a-digit-2-a
            led-deck-a-digit-2-b
            led-deck-a-digit-2-c
            led-deck-a-digit-2-d
            led-deck-a-digit-2-e
            led-deck-a-digit-2-f
            led-deck-a-digit-2-g
            led-deck-a-digit-2-dot
            wheel-deck-a
            fader-deck-a-tempo
            wheel-distance-deck-a
            led-deck-b-1-blue
            led-deck-b-1-green
            led-deck-b-2-blue
            led-deck-b-2-green
            led-deck-b-3-blue
            led-deck-b-3-green
            led-deck-b-4-blue
            led-deck-b-4-green
            led-deck-b-load
            led-deck-b-deck-d-button
            led-deck-b-in
            led-deck-b-out
            led-deck-b-shift
            led-deck-b-sync
            led-deck-b-cue
            led-deck-b-play
            led-deck-b-tempo-up
            led-deck-b-tempo-down
            led-deck-b-master
            led-deck-b-keylock
            led-deck-b-deck-b
            led-deck-b-deck-d
            led-deck-b-samples
            led-deck-b-on-air
            led-deck-b-sample-1
            led-deck-b-sample-2
            led-deck-b-sample-3
            led-deck-b-sample-4
            led-deck-b-digit-1-a
            led-deck-b-digit-1-b
            led-deck-b-digit-1-c
            led-deck-b-digit-1-d
            led-deck-b-digit-1-e
            led-deck-b-digit-1-f
            led-deck-b-digit-1-g
            led-deck-b-digit-1-dot
            led-deck-b-digit-2-a
            led-deck-b-digit-2-b
            led-deck-b-digit-2-c
            led-deck-b-digit-2-d
            led-deck-b-digit-2-e
            led-deck-b-digit-2-f
            led-deck-b-digit-2-g
            led-deck-b-digit-2-dot
            wheel-deck-b
            fader-deck-b-tempo
            wheel-distance-deck-b
            led-fx1-dry/wet
            led-fx1-1
            led-fx1-2
            led-fx1-3
            led-fx1-mode
            led-fx2-dry/wet
            led-fx2-1
            led-fx2-2
            led-fx2-3
            led-fx2-mode
            knob-fx1-dry/wet
            knob-fx1-1
            knob-fx1-2
            knob-fx1-3
            knob-fx2-dry/wet
            knob-fx2-1
            knob-fx2-2
            knob-fx2-3
            button-unknown-1
            button-unknown-2
            button-unknown-3
            button-unknown-4
            button-unknown-5
            button-unknown-6
            button-unknown-7
            button-unknown-8
            button-unknown-9
            button-unknown-10
            button-unknown-11
            button-unknown-12
            button-unknown-13
            button-unknown-14
            button-unknown-15
            button-unknown-16
            button-unknown-17
            button-unknown-18
            button-unknown-19
            button-unknown-20
            button-unknown-21
            button-unknown-22
            button-unknown-23
            button-unknown-24
            button-unknown-25
            button-unknown-26
            button-unknown-27
            button-unknown-28
            button-unknown-29
            button-unknown-30
            button-unknown-31
            button-unknown-32
            button-unknown-33
            button-unknown-34
            button-unknown-35
            button-unknown-36
            button-unknown-37
            button-unknown-38
            button-unknown-39
            button-unknown-40
            knob-unknown-1
            knob-unknown-2
            knob-unknown-3
            knob-unknown-4
            knob-unknown-5
            knob-unknown-6
            knob-unknown-7
            knob-unknown-8
            knob-unknown-9
            pot-max
            knob-max))

(define alsa-name "TraktorKontrolS")
(define input-name "Traktor Kontrol S4")

(define-syntax-rule (abs x)
  (+ x abs-hat0x))

(define-syntax-rule (button x)
  (+ x btn-misc))

;; Master
(define-alsa-ctl led-master-quant 1 'integer)
(define-alsa-ctl led-master-headphone 2 'integer)
(define-alsa-ctl led-master-master 3 'integer)
(define-alsa-ctl led-master-snap 4 'integer)
(define-alsa-ctl led-master-warning 5 'integer)
(define-alsa-ctl led-master-master-button 6 'integer)
(define-alsa-ctl led-master-snap-button 7 'integer)
(define-alsa-ctl led-master-rec 8 'integer)
(define-alsa-ctl led-master-size 9 'integer)
(define-alsa-ctl led-master-quant-button 10 'integer)
(define-alsa-ctl led-master-browser-button 11 'integer)
(define-alsa-ctl led-master-play-button 12 'integer)
(define-alsa-ctl led-master-undo-button 13 'integer)

(define-input-event knob-loop-dry/wet ev-abs (abs 4))
(define-input-event fader-crossfader ev-abs (abs 7))
(define-input-event knob-mic-volume ev-abs (abs 8))
(define-input-event knob-cue-mix ev-abs (abs 9))

;; Chan A
(define-alsa-ctl led-chan-a-> 14 'integer)
(define-alsa-ctl led-chan-a-< 15 'integer)
(define-alsa-ctl led-chan-a-meter-1 16 'integer)
(define-alsa-ctl led-chan-a-meter-2 17 'integer)
(define-alsa-ctl led-chan-a-meter-3 18 'integer)
(define-alsa-ctl led-chan-a-meter-4 19 'integer)
(define-alsa-ctl led-chan-a-meter-5 20 'integer)
(define-alsa-ctl led-chan-a-meter-6 21 'integer)
(define-alsa-ctl led-chan-a-meter-clip 22 'integer)
(define-alsa-ctl led-chan-a-active 23 'integer)
(define-alsa-ctl led-chan-a-cue 24 'integer)
(define-alsa-ctl led-chan-a-fx1 25 'integer)
(define-alsa-ctl led-chan-a-fx2 26 'integer)

(define-input-event fader-chan-a ev-abs (abs 2))
(define-input-event knob-chan-a-eq-filter ev-abs (abs 24))
(define-input-event fader-chan-a-eq-low ev-abs (abs 25))
(define-input-event fader-chan-a-eq-mid ev-abs (abs 26))
(define-input-event fader-chan-a-eq-hi ev-abs (abs 27))

;; Chan B
(define-alsa-ctl led-chan-b-> 27 'integer)
(define-alsa-ctl led-chan-b-< 28 'integer)
(define-alsa-ctl led-chan-b-meter-1 29 'integer)
(define-alsa-ctl led-chan-b-meter-2 30 'integer)
(define-alsa-ctl led-chan-b-meter-3 31 'integer)
(define-alsa-ctl led-chan-b-meter-4 32 'integer)
(define-alsa-ctl led-chan-b-meter-5 33 'integer)
(define-alsa-ctl led-chan-b-meter-6 34 'integer)
(define-alsa-ctl led-chan-b-meter-clip 35 'integer)
(define-alsa-ctl led-chan-b-active 36 'integer)
(define-alsa-ctl led-chan-b-cue 37 'integer)
(define-alsa-ctl led-chan-b-fx1 38 'integer)
(define-alsa-ctl led-chan-b-fx2 39 'integer)

(define-input-event fader-chan-b ev-abs (abs 1))
(define-input-event knob-chan-b-eq-filter ev-abs (abs 20))
(define-input-event fader-chan-b-eq-low ev-abs (abs 21))
(define-input-event fader-chan-b-eq-mid ev-abs (abs 22))
(define-input-event fader-chan-b-eq-hi ev-abs (abs 23))

;; Chan C
(define-alsa-ctl led-chan-c-> 40 'integer)
(define-alsa-ctl led-chan-c-< 41 'integer)
(define-alsa-ctl led-chan-c-meter-1 42 'integer)
(define-alsa-ctl led-chan-c-meter-2 43 'integer)
(define-alsa-ctl led-chan-c-meter-3 44 'integer)
(define-alsa-ctl led-chan-c-meter-4 45 'integer)
(define-alsa-ctl led-chan-c-meter-5 46 'integer)
(define-alsa-ctl led-chan-c-meter-6 47 'integer)
(define-alsa-ctl led-chan-c-meter-clip 48 'integer)
(define-alsa-ctl led-chan-c-active 49 'integer)
(define-alsa-ctl led-chan-c-cue 50 'integer)
(define-alsa-ctl led-chan-c-fx1 51 'integer)
(define-alsa-ctl led-chan-c-fx2 52 'integer)

(define-input-event fader-chan-c ev-abs (abs 3))
(define-input-event knob-chan-c-eq-filter ev-abs (abs 28))
(define-input-event fader-chan-c-eq-low ev-abs (abs 29))
(define-input-event fader-chan-c-eq-mid ev-abs (abs 30))
(define-input-event fader-chan-c-eq-hi ev-abs (abs 31))

;; Chan D
(define-alsa-ctl led-chan-d-> 53 'integer)
(define-alsa-ctl led-chan-d-< 54 'integer)
(define-alsa-ctl led-chan-d-meter-1 55 'integer)
(define-alsa-ctl led-chan-d-meter-2 56 'integer)
(define-alsa-ctl led-chan-d-meter-3 57 'integer)
(define-alsa-ctl led-chan-d-meter-4 58 'integer)
(define-alsa-ctl led-chan-d-meter-5 59 'integer)
(define-alsa-ctl led-chan-d-meter-6 60 'integer)
(define-alsa-ctl led-chan-d-meter-clip 61 'integer)
(define-alsa-ctl led-chan-d-active 62 'integer)
(define-alsa-ctl led-chan-d-cue 63 'integer)
(define-alsa-ctl led-chan-d-fx1 64 'integer)
(define-alsa-ctl led-chan-d-fx2 65 'integer)

(define-input-event fader-chan-d ev-abs (abs 0))
(define-input-event knob-chan-d-eq-filter ev-abs (abs 12))
(define-input-event fader-chan-d-eq-low ev-abs (abs 13))
(define-input-event fader-chan-d-eq-mid ev-abs (abs 14))
(define-input-event fader-chan-d-eq-hi ev-abs (abs 15))

;; Deck A
(define-alsa-ctl led-deck-a-1-blue 66 'integer)
(define-alsa-ctl led-deck-a-1-green 67 'integer)
(define-alsa-ctl led-deck-a-2-blue 68 'integer)
(define-alsa-ctl led-deck-a-2-green 69 'integer)
(define-alsa-ctl led-deck-a-3-blue 70 'integer)
(define-alsa-ctl led-deck-a-3-green 71 'integer)
(define-alsa-ctl led-deck-a-4-blue 72 'integer)
(define-alsa-ctl led-deck-a-4-green 73 'integer)
(define-alsa-ctl led-deck-a-load 74 'integer)
(define-alsa-ctl led-deck-a-deck-c-button 7 'integer5)
(define-alsa-ctl led-deck-a-in 76 'integer)
(define-alsa-ctl led-deck-a-out 77 'integer)
(define-alsa-ctl led-deck-a-shift 78 'integer)
(define-alsa-ctl led-deck-a-sync 79 'integer)
(define-alsa-ctl led-deck-a-cue 80 'integer)
(define-alsa-ctl led-deck-a-play 81 'integer)
(define-alsa-ctl led-deck-a-tempo-up 82 'integer)
(define-alsa-ctl led-deck-a-tempo-down 83 'integer)
(define-alsa-ctl led-deck-a-master 84 'integer)
(define-alsa-ctl led-deck-a-keylock 85 'integer)
(define-alsa-ctl led-deck-a-deck-a 86 'integer)
(define-alsa-ctl led-deck-a-deck-c 87 'integer)
(define-alsa-ctl led-deck-a-samples 88 'integer)
(define-alsa-ctl led-deck-a-on-air 89 'integer)
(define-alsa-ctl led-deck-a-sample-1 90 'integer)
(define-alsa-ctl led-deck-a-sample-2 91 'integer)
(define-alsa-ctl led-deck-a-sample-3 92 'integer)
(define-alsa-ctl led-deck-a-sample-4 93 'integer)
(define-alsa-ctl led-deck-a-digit-1-a 94 'integer)
(define-alsa-ctl led-deck-a-digit-1-b 95 'integer)
(define-alsa-ctl led-deck-a-digit-1-c 96 'integer)
(define-alsa-ctl led-deck-a-digit-1-d 97 'integer)
(define-alsa-ctl led-deck-a-digit-1-e 98 'integer)
(define-alsa-ctl led-deck-a-digit-1-f 99 'integer)
(define-alsa-ctl led-deck-a-digit-1-g 100 'integer)
(define-alsa-ctl led-deck-a-digit-1-dot 101 'integer)
(define-alsa-ctl led-deck-a-digit-2-a 102 'integer)
(define-alsa-ctl led-deck-a-digit-2-b 103 'integer)
(define-alsa-ctl led-deck-a-digit-2-c 104 'integer)
(define-alsa-ctl led-deck-a-digit-2-d 105 'integer)
(define-alsa-ctl led-deck-a-digit-2-e 106 'integer)
(define-alsa-ctl led-deck-a-digit-2-f 107 'integer)
(define-alsa-ctl led-deck-a-digit-2-g 108 'integer)
(define-alsa-ctl led-deck-a-digit-2-dot 109 'integer)

(define-input-event wheel-deck-a ev-abs (abs 36))
(define-input-event fader-deck-a-tempo ev-abs (abs 5))
(define-input-event wheel-distance-deck-a ev-abs (abs 10))

;; Deck B
(define-alsa-ctl led-deck-b-1-blue 110 'integer)
(define-alsa-ctl led-deck-b-1-green 111 'integer)
(define-alsa-ctl led-deck-b-2-blue 112 'integer)
(define-alsa-ctl led-deck-b-2-green 113 'integer)
(define-alsa-ctl led-deck-b-3-blue 114 'integer)
(define-alsa-ctl led-deck-b-3-green 115 'integer)
(define-alsa-ctl led-deck-b-4-blue 116 'integer)
(define-alsa-ctl led-deck-b-4-green 117 'integer)
(define-alsa-ctl led-deck-b-load 118 'integer)
(define-alsa-ctl led-deck-b-deck-d-button 119 'integer)
(define-alsa-ctl led-deck-b-in 120 'integer)
(define-alsa-ctl led-deck-b-out 121 'integer)
(define-alsa-ctl led-deck-b-shift 122 'integer)
(define-alsa-ctl led-deck-b-sync 123 'integer)
(define-alsa-ctl led-deck-b-cue 124 'integer)
(define-alsa-ctl led-deck-b-play 125 'integer)
(define-alsa-ctl led-deck-b-tempo-up 126 'integer)
(define-alsa-ctl led-deck-b-tempo-down 127 'integer)
(define-alsa-ctl led-deck-b-master 128 'integer)
(define-alsa-ctl led-deck-b-keylock 129 'integer)
(define-alsa-ctl led-deck-b-deck-b 130 'integer)
(define-alsa-ctl led-deck-b-deck-d 131 'integer)
(define-alsa-ctl led-deck-b-samples 132 'integer)
(define-alsa-ctl led-deck-b-on-air 133 'integer)
(define-alsa-ctl led-deck-b-sample-1 134 'integer)
(define-alsa-ctl led-deck-b-sample-2 135 'integer)
(define-alsa-ctl led-deck-b-sample-3 136 'integer)
(define-alsa-ctl led-deck-b-sample-4 137 'integer)
(define-alsa-ctl led-deck-b-digit-1-a 138 'integer)
(define-alsa-ctl led-deck-b-digit-1-b 139 'integer)
(define-alsa-ctl led-deck-b-digit-1-c 140 'integer)
(define-alsa-ctl led-deck-b-digit-1-d 141 'integer)
(define-alsa-ctl led-deck-b-digit-1-e 142 'integer)
(define-alsa-ctl led-deck-b-digit-1-f 143 'integer)
(define-alsa-ctl led-deck-b-digit-1-g 144 'integer)
(define-alsa-ctl led-deck-b-digit-1-dot 145 'integer)
(define-alsa-ctl led-deck-b-digit-2-a 146 'integer)
(define-alsa-ctl led-deck-b-digit-2-b 147 'integer)
(define-alsa-ctl led-deck-b-digit-2-c 148 'integer)
(define-alsa-ctl led-deck-b-digit-2-d 149 'integer)
(define-alsa-ctl led-deck-b-digit-2-e 150 'integer)
(define-alsa-ctl led-deck-b-digit-2-f 151 'integer)
(define-alsa-ctl led-deck-b-digit-2-g 152 'integer)
(define-alsa-ctl led-deck-b-digit-2-dot 153 'integer)

(define-input-event wheel-deck-b ev-abs (abs 37))
(define-input-event fader-deck-b-tempo ev-abs (abs 6))
(define-input-event wheel-distance-deck-b ev-abs (abs 11))

;; FX
(define-alsa-ctl led-fx1-dry/wet 154 'integer)
(define-alsa-ctl led-fx1-1 155 'integer)
(define-alsa-ctl led-fx1-2 156 'integer)
(define-alsa-ctl led-fx1-3 157 'integer)
(define-alsa-ctl led-fx1-mode 158 'integer)
(define-alsa-ctl led-fx2-dry/wet 159 'integer)
(define-alsa-ctl led-fx2-1 160 'integer)
(define-alsa-ctl led-fx2-2 161 'integer)
(define-alsa-ctl led-fx2-3 162 'integer)
(define-alsa-ctl led-fx2-mode 163 'integer)

(define-input-event knob-fx1-dry/wet ev-abs (abs 32))
(define-input-event knob-fx1-1 ev-abs (abs 33))
(define-input-event knob-fx1-2 ev-abs (abs 34))
(define-input-event knob-fx1-3 ev-abs (abs 35))
(define-input-event knob-fx2-dry/wet ev-abs (abs 16))
(define-input-event knob-fx2-1 ev-abs (abs 17))
(define-input-event knob-fx2-2 ev-abs (abs 18))
(define-input-event knob-fx2-3 ev-abs (abs 19))

;; 40 buttons of unknown description.  They start from key code
;; BTN_MISC (256)
(define-input-event button-unknown-1 ev-key (button 0))
(define-input-event button-unknown-2 ev-key (button 1))
(define-input-event button-unknown-3 ev-key (button 2))
(define-input-event button-unknown-4 ev-key (button 3))
(define-input-event button-unknown-5 ev-key (button 4))
(define-input-event button-unknown-6 ev-key (button 5))
(define-input-event button-unknown-7 ev-key (button 6))
(define-input-event button-unknown-8 ev-key (button 7))
(define-input-event button-unknown-9 ev-key (button 8))
(define-input-event button-unknown-10 ev-key (button 9))
(define-input-event button-unknown-11 ev-key (button 10))
(define-input-event button-unknown-12 ev-key (button 11))
(define-input-event button-unknown-13 ev-key (button 12))
(define-input-event button-unknown-14 ev-key (button 13))
(define-input-event button-unknown-15 ev-key (button 14))
(define-input-event button-unknown-16 ev-key (button 15))
(define-input-event button-unknown-17 ev-key (button 16))
(define-input-event button-unknown-18 ev-key (button 17))
(define-input-event button-unknown-19 ev-key (button 18))
(define-input-event button-unknown-20 ev-key (button 19))
(define-input-event button-unknown-21 ev-key (button 20))
(define-input-event button-unknown-22 ev-key (button 21))
(define-input-event button-unknown-23 ev-key (button 22))
(define-input-event button-unknown-24 ev-key (button 23))
(define-input-event button-unknown-25 ev-key (button 24))
(define-input-event button-unknown-26 ev-key (button 25))
(define-input-event button-unknown-27 ev-key (button 26))
(define-input-event button-unknown-28 ev-key (button 27))
(define-input-event button-unknown-29 ev-key (button 28))
(define-input-event button-unknown-30 ev-key (button 29))
(define-input-event button-unknown-31 ev-key (button 30))
(define-input-event button-unknown-32 ev-key (button 31))
(define-input-event button-unknown-33 ev-key (button 32))
(define-input-event button-unknown-34 ev-key (button 33))
(define-input-event button-unknown-35 ev-key (button 34))
(define-input-event button-unknown-36 ev-key (button 35))
(define-input-event button-unknown-37 ev-key (button 36))
(define-input-event button-unknown-38 ev-key (button 37))
(define-input-event button-unknown-39 ev-key (button 38))
(define-input-event button-unknown-40 ev-key (button 39))

;; 9 knobs of unknown description.  They start from abs code
;; ABS_HAT0X+38=54
(define-input-event knob-unknown-1 ev-abs (abs 38))
(define-input-event knob-unknown-2 ev-abs (abs 39))
(define-input-event knob-unknown-3 ev-abs (abs 40))
(define-input-event knob-unknown-4 ev-abs (abs 41))
(define-input-event knob-unknown-5 ev-abs (abs 42))
(define-input-event knob-unknown-6 ev-abs (abs 43))
(define-input-event knob-unknown-7 ev-abs (abs 44))
(define-input-event knob-unknown-8 ev-abs (abs 45))
(define-input-event knob-unknown-9 ev-abs (abs 46))

;; Constants
(define pot-max (make-input-max-parameter 4096))
(define knob-max (make-input-max-parameter 15))
