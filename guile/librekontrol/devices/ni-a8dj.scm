;; Copyright (C) 2019  Brandon M. Invergo <brandon@invergo.net>

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (librekontrol devices ni-a8dj)
  #:use-module (librekontrol alsa)
  #:export (alsa-name
            input-name
            led-current-input
            led-gnd-lift-vinyl
            led-gnd-lift-cd
            led-gnd-lift-phono
            led-software-lock))

(define alsa-name "Audio8DJ")
(define input-name #f)

(define-alsa-ctl led-current-input 1 'integer)
(define-alsa-ctl led-gnd-lift-vinyl 2 'boolean)
(define-alsa-ctl led-gnd-lift-cd 3 'boolean)
(define-alsa-ctl led-gnd-lift-phono 4 'boolean)
(define-alsa-ctl led-software-lock 5 'boolean)
