;; Copyright (C) 2019, 2020  Brandon M. Invergo <brandon@invergo.net>

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (librekontrol devices ni-rk2)
  #:use-module (librekontrol input)
  #:use-module (librekontrol alsa)
  #:use-module (librekontrol device)
  #:export (alsa-name
            input-name
            led-1
            led-2
            led-3
            led-4
            led-5
            led-6
            led-pedal
            button-1-in
            button-2-in
            button-3-in
            button-4-in
            button-5-in
            button-6-in
            button-pedal
            button-1
            button-2
            button-3
            button-4
            button-5
            button-6
            pedal
            led-7seg-1b
            led-7seg-1c
            led-7seg-2a
            led-7seg-2b
            led-7seg-2c
            led-7seg-2d
            led-7seg-2e
            led-7seg-2f
            led-7seg-2g
            led-7seg-3a
            led-7seg-3b
            led-7seg-3c
            led-7seg-3d
            led-7seg-3e
            led-7seg-3f
            led-7seg-3g
            trs-pedal-in-1
            trs-pedal-in-2
            pedal-expression
            pedal-in-1
            pedal-in-2
            expression
            trs-max))

(define alsa-name "RigKontrol2")
(define input-name "RigKontrol2")

;; Main
(define-alsa-ctl led-1 6 'boolean)
(define-alsa-ctl led-2 5 'boolean)
(define-alsa-ctl led-3 4 'boolean)
(define-alsa-ctl led-4 3 'boolean)
(define-alsa-ctl led-5 2 'boolean)
(define-alsa-ctl led-6 1 'boolean)
(define-alsa-ctl led-pedal 7 'boolean)

(define-input-event button-1-in ev-key key-1)
(define-input-event button-2-in ev-key key-2)
(define-input-event button-3-in ev-key key-3)
(define-input-event button-4-in ev-key key-4)
(define-input-event button-5-in ev-key key-5)
(define-input-event button-6-in ev-key key-6)
(define-input-event button-pedal ev-key key-7)

(define-control button-1 button-1-in led-1)
(define-control button-2 button-2-in led-2)
(define-control button-3 button-3-in led-3)
(define-control button-4 button-4-in led-4)
(define-control button-5 button-5-in led-5)
(define-control button-6 button-6-in led-6)
(define-control pedal button-pedal led-pedal)

;; 7-segment LED
(define-alsa-ctl led-7seg-1b 9 'boolean)
(define-alsa-ctl led-7seg-1c 10 'boolean)

(define-alsa-ctl led-7seg-2a 11 'boolean)
(define-alsa-ctl led-7seg-2b 12 'boolean)
(define-alsa-ctl led-7seg-2c 13 'boolean)
(define-alsa-ctl led-7seg-2d 14 'boolean)
(define-alsa-ctl led-7seg-2e 15 'boolean)
(define-alsa-ctl led-7seg-2f 16 'boolean)
(define-alsa-ctl led-7seg-2g 17 'boolean)

(define-alsa-ctl led-7seg-3a 18 'boolean)
(define-alsa-ctl led-7seg-3b 19 'boolean)
(define-alsa-ctl led-7seg-3c 20 'boolean)
(define-alsa-ctl led-7seg-3d 21 'boolean)
(define-alsa-ctl led-7seg-3e 22 'boolean)
(define-alsa-ctl led-7seg-3f 23 'boolean)
(define-alsa-ctl led-7seg-3g 24 'boolean)

;; Knobs
(define-input-event trs-pedal-in-1 ev-abs abs-x)
(define-input-event trs-pedal-in-2 ev-abs abs-y)
(define-input-event pedal-expression ev-abs abs-z)

(define-control pedal-in-1 trs-pedal-in-1 #f)
(define-control pedal-in-2 trs-pedal-in-2 #f)
(define-control expression pedal-expression #f)

(define trs-max (make-input-max-parameter 4096))
